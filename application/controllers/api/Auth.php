<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Auth extends BD_Controller {

    
    function __construct()
    {
     //echo 'test';exit;
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('M_main');
        //$this->load->helper(['jwt_helper']);
       
        
    }

    
    
    public function login_post()
    {
        //print_r('test');exit;
        $u = $this->post('username'); //Username Posted
        $p = sha1($this->post('password')); //Pasword Posted
        $q = array('username' => $u); //For where query condition
        $kunci = $this->config->item('thekey');
        $invalidLogin = ['status' => 'Invalid Login']; //Respon if login invalid
        $val = $this->M_main->get_user($q)->row(); //Model to get single data row from database base on username
        if($this->M_main->get_user($q)->num_rows() == 0){$this->response($invalidLogin, REST_Controller::HTTP_NOT_FOUND);}
		$match = $val->password;   //Get password for user from database
        if($p == $match){  //Condition if password matched
        	$token['id'] = $val->id;  //From here
            $token['username'] = $u;
            $token['first_name'] = 'puviyarasan';
            $token['last_name'] = 's';
            $token['mobile_number'] = '945343453';
            $date = new DateTime();
            $token['iat'] = $date->getTimestamp();
            $token['exp'] = $date->getTimestamp() + 60; //To here is to generate token
            $output['token'] = JWT::encode($token,$kunci ); //This is the output token
            $keyval = $this->set_response($output, REST_Controller::HTTP_OK); //This is the respon if success
            //print_r($output_token);exit;
            }
        else {
            $this->set_response($invalidLogin, REST_Controller::HTTP_NOT_FOUND); //This is the respon if failed
        }
    }
    
    public function check_token_get()
    {
        $kunci = $this->config->item('thekey'); //secret key for encode and decode
        $headers = $this->input->get_request_header('Authorization'); //get token from request header

        try {
           $decoded = JWT::decode($headers, $kunci, array('HS256'));
           $decoded_array = (array) $decoded;
           $this->set_response($decoded, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $invalid = ['status' => $e->getMessage()]; //Respon if credential invalid
             $this->set_response($invalid, REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    
     public function refresh_get() {
        $kunci = $this->config->item('thekey');
        $token = $this->input->get_request_header('Authorization');
         try{
             
             $decodedexception = JWT::decode($token, $kunci, ['HS256']);
             
             
             //TODO: do something if exception is not fired
         }catch (Firebase\JWT\ExpiredException $e ) {
             
             JWT::$leeway = 5000;
             $decodedexception = (array) JWT::decode($token, $kunci, ['HS256']);
             
             // TODO: test if token is blacklisted
             $date = new DateTime();
                $decodedexception['iat'] = $date->getTimestamp();
                $decodedexception['exp'] = $date->getTimestamp() + 60;
                          
             $keyval = JWT::encode($decodedexception, $kunci);
             print_r($keyval);exit;
         }catch (Exception $e ){
             var_dump($e);
         }
     }
    
}


